var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');

//variables mongodb
var mongoClient = require('mongodb').MongoClient;
//var url ="mongodb://localhost:27017/local"; SIN DOCKERIZAR
//variable url cambiar por nombre de la network si dockerizamos mongo
var url ="mongodb://servermongo:27017/local";

//variables MLAB
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos";
var apiKey = "apiKey=jEx6OlQOJmUHZeZBARrkqhO-eEeuOlTK";
var clienteMlab = requestjson.createClient(urlmovimientosMlab + apiKey);

// variables POSTGRE
var pg = require('pg');
//indicar con usuario y contraseña
var urlUsusarios = "postgres://docker:docker@localhost:5433/bdseguridad";
var clientePostgre = new pg.Client (urlUsusarios);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//mongodb BBDD de movimientos

app.get('/getmongomovi', function(req, res){

mongoClient.connect(url, function(err, db){
if (err)
{
console.log(err);
}
else
{
var col = db.collection('movimientos');
console.log("Connected successfully to server");
col.find({}).limit(3).toArray(function(err, docs){
//JSON.stringify(docs)
//en el front al recuperar JSON.parse(xxxxxx)
res.send(docs);
});
db.close();
res.send("ok");
}
})
});


// postgres bbdd usuarios

clientePostgre.connect();

//Asumo que recigo esto req.body = {usuario:xx, password:yyyy}
//Ejecuto la query de consulta
app.post('/login', function(req,res){
//clientePostgre.connect();
//var clientePostgre = new pg.Client (urlUsusarios);
const query = clientePostgre.query('SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password=$2;', [req.body.login, req.body.password],(err,result) => {
  if (err){
    console.log(err);
    res.send(err);
  }
  else {
    if (result.rows[0].count >= 1){
      res.send("login correcto");
    }
    else {
      res.send("login incorrecto");
    }
    //console.log(result.row[0]);
    //res.send(result.rows[0]);
  }
});
//devolver el restultado
});


/*
app.post('/postmongomovi', function(req, res) {
  mongoClient.connect(url,function(err, db){
    if (err)
    {
      console.log(err);
    }
    else
    {
    console.log("traying to insert to server");
    var col = db.collection('prueba');
    db.collection('prueba').insertOne({a:1}), function(err, r){
      console.log(r.insertedCount + 'registros insertados');
    /*
    db.collection('prueba').insertMany([{a:2,a:3}]), function(err, r){
      console.log(r.insertedCount + 'registros insertados');
    });

    db.collection('prueba').insertOne(req.body), function(err, r){
      console.log(r.insertedCount + 'registros insertados');
    });

    }
    db.close();
    res.send("ok");
  }
})
}); */

app.get('/movimientos', function(req, res) {
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/clientes', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab = requestjson.createClient(urlmovimientosMlab + "?f={'idcliente':1, 'nombre': 1, 'apellidos': 1}" + "&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      console.log(body);
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/movimientos', function(req, res) {
  /* Crear movimiento en MLab */
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);
  clienteMlab.post('',req.body,function(err, resM, body){
  if(err)
  {
    console.log(body);
  }
  else
  {
    res.send(body);
  }
});
});

app.get('/movimientos/:idcliente', function (req, res) {
  /* Obtener movimientos del cliente idcliente desde MLab */
})
